// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FPSAIGuard.generated.h"


UENUM(BlueprintType)
enum class EAIState : uint8
{
	Idle,

	Suspect,

	Alert
};



class UPawnSensingComponent;

UCLASS()
class FPSGAME_API AFPSAIGuard : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AFPSAIGuard();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "components")
	UPawnSensingComponent* PawnSensing;

	UFUNCTION()
	void OnPawnSee(APawn* Pawn);

	UFUNCTION()
	void OnPawnHear(APawn* pawn, const FVector& Location, float Volume);

	UPROPERTY(BlueprintReadWrite, Category = "components")
	FRotator OriginalRotation;

	FTimerHandle TimerHandle_ResetOrientation;
	void ResetOrientation();

	UPROPERTY(ReplicatedUsing=OnRep_GuardState)
	EAIState GuardState;

	UFUNCTION()
	void OnRep_GuardState();

	void SetGuardState(EAIState new_state);

	UFUNCTION(BlueprintImplementableEvent, Category = "AI")
	void OnStateChange(EAIState new_state);


protected:
	UPROPERTY(EditInstanceOnly, Category = "AI")
	bool bGuard;


	UPROPERTY(EditInstanceOnly, Category = "AI")
	bool bPatrol;

	UPROPERTY(EditInstanceOnly, Category = "AI", meta = (EditCondition = "bPatrol"))
	AActor* FirstPatrolPoint;

	UPROPERTY(EditInstanceOnly, Category = "AI", meta = (EditCondition = "bPatrol"))
	AActor* SecondPatrolPoint;

	AActor* CurrentPatrolPoint;

	void MoveToNextPatrolPoint();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
};
