// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSExractionZone.h"
#include "Components/BoxComponent.h"
#include "Components/DecalComponent.h"
#include "FPSCharacter.h"
#include "FPSGameMode.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
AFPSExractionZone::AFPSExractionZone()
{
	OvelrapComp = CreateDefaultSubobject<UBoxComponent>(TEXT("OverlapComp"));
	OvelrapComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	OvelrapComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	OvelrapComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	OvelrapComp->SetBoxExtent(FVector(200.f));

	OvelrapComp->SetHiddenInGame(false);

	RootComponent = OvelrapComp;

	OvelrapComp->OnComponentBeginOverlap.AddDynamic(this, &AFPSExractionZone::HandleOverlap);


	DecalComp = CreateDefaultSubobject<UDecalComponent>(TEXT("DecalComp"));
	DecalComp->DecalSize = FVector(200.0f);
	DecalComp->SetupAttachment(RootComponent);
}



void AFPSExractionZone::HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	AFPSCharacter* pawn = Cast<AFPSCharacter>(OtherActor);

	if (pawn == nullptr) 
		return;

	if (pawn->bIsCarryingObjective) {
		AFPSGameMode * game_mode = Cast<AFPSGameMode>( GetWorld()->GetAuthGameMode() );

		if (game_mode)
			game_mode->CompleteMisson(pawn, true);
	}
	else 
	{
		UGameplayStatics::PlaySound2D(this, ObjectiveMissingSound);
	}

}




