// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSAIGuard.h"
#include "Perception/PawnSensingComponent.h"
#include "DrawDebugHelpers.h"
#include "FPSGameMode.h"
#include "AI/Navigation/NavigationSystem.h"
#include "Net/UnrealNetwork.h"



// Sets default values
AFPSAIGuard::AFPSAIGuard()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	PawnSensing = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));


	PawnSensing->OnSeePawn.AddDynamic(this, &AFPSAIGuard::OnPawnSee);
	PawnSensing->OnHearNoise.AddDynamic(this, &AFPSAIGuard::OnPawnHear);

	GuardState = EAIState::Idle;
	bGuard = true;
}


void AFPSAIGuard::OnPawnSee(APawn* pawn)
{
	if (pawn == nullptr || !bGuard)
		return;

	DrawDebugSphere(GetWorld(), pawn->GetActorLocation(), 32.0f, 12, FColor::Yellow, false, 10.0f);


	AFPSGameMode * game_mode = Cast<AFPSGameMode>(GetWorld()->GetAuthGameMode());

	if (game_mode)
		game_mode->CompleteMisson(pawn, false);

	SetGuardState(EAIState::Alert);


	AController* Controller = GetController();
	if (Controller)
	{
		Controller->StopMovement();
	}
}


void AFPSAIGuard::OnPawnHear(APawn* pawn, const FVector& Location, float Volume)
{
	if (GuardState == EAIState::Alert || !bGuard)
	{
		return;
	}


	if (pawn == nullptr)
		return;

	DrawDebugSphere(GetWorld(), Location, 32.0f, 12, FColor::Red, false, 10.0f);

	FVector Direction = Location - GetActorLocation();
	Direction.Normalize();
	

	FRotator NewLookAt = FRotationMatrix::MakeFromX(Direction).Rotator();
	NewLookAt.Pitch = 0;
	NewLookAt.Roll = 0;

	SetActorRotation(NewLookAt);

	
	GetWorldTimerManager().ClearTimer(TimerHandle_ResetOrientation);
	GetWorldTimerManager().SetTimer(TimerHandle_ResetOrientation, this, &AFPSAIGuard::ResetOrientation, 3.0f, false);

	SetGuardState(EAIState::Suspect);


	AController* Controller = GetController();
	if (Controller)
	{
		Controller->StopMovement();
	}
}


void AFPSAIGuard::ResetOrientation()
{
	if (GuardState == EAIState::Alert)
	{
		return;
	}


	SetActorRotation(OriginalRotation);

	SetGuardState(EAIState::Idle);

	if (bPatrol)
	{
		MoveToNextPatrolPoint();
	}
}


void AFPSAIGuard::OnRep_GuardState()
{
	OnStateChange(GuardState);
}


void AFPSAIGuard::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AFPSAIGuard, GuardState);
}


void AFPSAIGuard::SetGuardState(EAIState new_state)
{
	if (GuardState != new_state) {
		GuardState = new_state;

		OnRep_GuardState();
	}
}

void AFPSAIGuard::MoveToNextPatrolPoint()
{
	if (CurrentPatrolPoint == nullptr || CurrentPatrolPoint == SecondPatrolPoint)
	{
		CurrentPatrolPoint = FirstPatrolPoint;
	}
	else
	{
		CurrentPatrolPoint = SecondPatrolPoint;
	}


	UNavigationSystem::SimpleMoveToActor(GetController(), CurrentPatrolPoint);
}


// Called when the game starts or when spawned
void AFPSAIGuard::BeginPlay()
{
	Super::BeginPlay();

	OriginalRotation = GetActorRotation();

	if (bPatrol)
	{
		MoveToNextPatrolPoint();
	}
}

// Called every frame
void AFPSAIGuard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CurrentPatrolPoint)
	{
		FVector Delta = GetActorLocation() - CurrentPatrolPoint->GetActorLocation();
		float DistanceToGoal = Delta.Size();

		//GEngine->AddOnScreenDebugMessage(-1, 0, FColor::Red, FString::Printf(TEXT("Distance: %f"), DistanceToGoal) );
		//UE_LOG(LogTemp, Warning, TEXT("DistanceToGoal reached!"));

		if (DistanceToGoal < 80)
		{
			

			MoveToNextPatrolPoint();
		}
			
	}

}

